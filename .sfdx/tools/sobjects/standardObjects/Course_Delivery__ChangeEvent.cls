// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Course_Delivery__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global String CurrencyIsoCode;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Date Start_Date__c;
    global String Region__c;
    global String Location__c;
    global User Instructor__c;
    global String Status__c;
    global Course__c Course__c;
    global Double Attendee_Count__c;
    global Date End_Date__c;

    global Course_Delivery__ChangeEvent () 
    {
    }
}