// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Course_Attendee__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global String CurrencyIsoCode;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    /* Enrolled - Student is enrolled in course delivery.
Completed - Student attended course delivery.
Course Cancelled - AW Computing cancelled course delivery.
No Show - Student did not show up for course delivery.
    */
    global String Status__c;
    global Contact Student__c;
    global Course_Delivery__c Course_Delivery__c;
    global String Student_Name__c;
    global String InstructorNotes__c;

    global Course_Attendee__ChangeEvent () 
    {
    }
}